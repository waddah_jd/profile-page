const gulp = require("gulp");
const sassConverter = require("gulp-sass");

const copyHtml = () => {
  return gulp.src("src/*.html").pipe(gulp.dest("public/"));
};

const sass = () => {
  return gulp
    .src("src/scss/**/*.scss")
    .pipe(sassConverter())
    .pipe(gulp.dest("public/css"));
};

const watchFiles = () => {
  return gulp.watch("src/scss/**/*.scss", sass);
};

exports.build = gulp.series(copyHtml, sass);
exports.dev = gulp.series(copyHtml, sass, watchFiles);
